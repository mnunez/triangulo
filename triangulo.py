# MARTA NÚÑEZ GARCÍA

#!/usr/bin/env pythoh3

# Se ha realizado un programa que acepta como argumento en la línea de comandos un número y muestra un triángulo
# de números, mostrando en primer lugar el número menor hasta el número introducido (número mayor) que forma el final
# del triángulo.

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int) -> str: # Función "line" que transforma el número entero introducido a caracter
                              # y lo multiplica por el número introducido con el fin de obtener el caracter n veces,
                              # donde n = numero introducido. De esta forma, se obtiene la cadena de caracteres
                              # deseada.
    """Return a string corresponding to the line for number"""
    return str(number) * number


def triangle(number: int) -> str: # Función "triangle" que si el número introducido es menor o igual que 9, aplica la
                                  # función "line" a cada valor del rango proporcionado. Se escribe cada iteración
                                  # del bucle "FOR" en la variable "lines", separado mediante espacios.
                                  # Si el número entero introducido es mayor que 9, imprime un mensaje por pantalla.
    """Return a string corresponding to the triangle for number"""
    if number <= 9:
        lines = ""
        for n in range(1, number + 1):
            t = line(n)
            lines = lines + t + "\n"
        return lines

    else:
        return f"El parámetro introducido {number} es mayor que 9."


def main(): # Función "main", es la función principal. Se lee un argumento en la línea de comandos (en este caso
            # un número entero) y se aplica la función "triangle" a dicho número. El resultado se muestra por pantalla.
            # Si se levanta la excepción "ValueError", el número leído como argumento es mayor que 9 y, por lo tanto,
            # llama a la función "triangle" con el valor 9.
    try:
        number: int = sys.argv[1]
        text = triangle(int(number))
        print(text)
    except ValueError:
        if int(number) > 9:
            print(triangle(9))


if __name__ == '__main__':
    main()